#include <mesh/interface.h>
#include <iostream>
#include <numbers>
#include <cstring>
#include <unordered_map>
#include <vector>

using  u32 = mesh:: u32;
using vec3 = mesh::vec3;

struct Cylinder : public mesh::Interface
{
    u32 dimR = 0u;
    u32 dimA = 0u;
    u32 dimZ = 0u;

    Cylinder() = default;
    Cylinder(u32 r, u32 a, u32 z)
        : mesh::Interface()
        , dimR(r)
        , dimA(a)
        , dimZ(z)
    {}

    vec3 point(u32 const cellI, u32 const faceI, u32 const pointI) const noexcept override
    {
        u32 const iz = cellI / (dimR * dimA);
        u32 const iy = (cellI - iz * dimZ) / dimR % dimA;
        u32 const ix = (cellI - iz * dimZ) % dimR;

        u32 const f[6][4] =
        {
            {0, 1, 3, 2},
            {4, 5, 7, 6},
            {0, 1, 5, 4},
            {2, 3, 7, 6},
            {0, 2, 6, 4},
            {1, 3, 7, 5},
        };
        u32 const i = f[faceI][pointI];

        double const x = static_cast<double>(ix +  (i % 2      != 0 ? 1 : 0));
        double const y = static_cast<double>(iy + ((i / 2) % 2 != 0 ? 1 : 0));
        double const z = static_cast<double>(iz +  (i / 4      != 0 ? 1 : 0));

        double const r = 0.8 * x / dimR + 0.2;
        double const phi = y == dimA
            ? 0.
            : (2. * std::numbers::pi) * (y / dimA);

        return vec3
        {
            r * std::cos(phi),
            r * std::sin(phi),
            z / dimZ,
        };
    }
    u32 pointCount(u32 const, u32 const) const noexcept override {return 4u;}
    u32 faceCount(u32 const) const noexcept override {return 6u;}
    u32 cellCount() const noexcept override {return dimR * dimA * dimZ;}
};

struct HashV3
{
    std::size_t operator()(vec3 const v) const noexcept
    {
        std::size_t a[3];
        static_assert(sizeof(a) == sizeof(v));

        std::memcpy(a, &v, sizeof(v));
        return a[0] + a[1] + a[2];
    }
};
struct EqualV3
{
    bool operator()(vec3 const v1, vec3 const v2) const noexcept
    {
        return v1.x == v2.x
            && v1.y == v2.y
            && v1.z == v2.z;
    }
};

std::ostream &operator<<(std::ostream &out, mesh::Interface const &mesh)
{
    std::unordered_map<vec3, u32, HashV3, EqualV3> point;
    for(u32 cellI = 0u; cellI < mesh.cellCount(); ++cellI)
    for(u32 faceI = 0u; faceI < mesh.faceCount(cellI); ++faceI)
    for(u32 pointI = 0u; pointI < mesh.pointCount(cellI, faceI); ++pointI)
    {
        vec3 const v = mesh.point(cellI, faceI, pointI);
        if(point.end() == point.find(v))
            point[v] = u32(point.size());
    }

    std::vector<vec3> p(point.size());
    for(auto const &[v, i] : point)
        p[i] = v;

    out << "o Mesh" << std::endl;
    for(auto const &v : p)
    {
        auto const &[x, y, z] = v;
        out << "v " << x << ' ' << y << ' ' << z << std::endl;
    }

    for(u32 cellI = 0u; cellI < mesh.cellCount(); ++cellI)
    for(u32 faceI = 0u; faceI < mesh.faceCount(cellI); ++faceI)
    {
        out << "f ";
        for(u32 pointI = 0u; pointI < mesh.pointCount(cellI, faceI); ++pointI)
            out << point[mesh.point(cellI, faceI, pointI)] + 1 << ' ';
        out << std::endl;
    }
    return out;
}

int main()
{
    Cylinder const cylinder = {10u, 20u, 3u};
    std::cout << cylinder << std::endl;
}
